import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AlertModule} from 'ng2-bootstrap';
import {ButtonModule} from 'primeng/primeng';
import { EtudiantComponent } from './personne/etudiant/etudiant/etudiant.component';
import { InviteComponent } from './personne/invite/invite/invite.component';
import { EnseignantComponent } from './personne/enseignant/enseignant/enseignant.component';
import { AdministrateurComponent } from './personne/administrateur/administrateur/administrateur.component';
import { NavbarComponent } from './shared/cadre/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    EtudiantComponent,
    InviteComponent,
    EnseignantComponent,
    AdministrateurComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    ButtonModule,
    AppRoutingModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
