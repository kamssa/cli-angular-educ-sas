import { browser, element, by } from 'protractor';

export class CliAngularEducSasPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('educ-root h1')).getText();
  }
}
