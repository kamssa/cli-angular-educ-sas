import { CliAngularEducSasPage } from './app.po';

describe('cli-angular-educ-sas App', () => {
  let page: CliAngularEducSasPage;

  beforeEach(() => {
    page = new CliAngularEducSasPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('educ works!');
  });
});
